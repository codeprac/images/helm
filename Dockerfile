# ref https://hub.docker.com/_/alpine
FROM alpine:3.14.2
SHELL ["/bin/ash", "-eo", "pipefail", "-c"]
RUN apk add --no-cache curl git jq make
WORKDIR /tmp

# ref https://github.com/helm/helm/releases
ARG HELM_3_VERSION=v3.7.0
RUN curl -fLo "./helm.tar.gz" "https://get.helm.sh/helm-${HELM_3_VERSION}-linux-amd64.tar.gz" \
  && tar xvzf "./helm.tar.gz" \
  && mv "./linux-amd64/helm" "/usr/local/bin/helm" \
  && chmod 750 "/usr/local/bin/helm" \
  && rm -rf "./linux-amd64" "./helm.tar.gz"

# ref https://github.com/mozilla/sops/releases
ARG SOPS_VERSION=v3.7.1
RUN curl -fLo "/usr/local/bin/sops" "https://github.com/mozilla/sops/releases/download/${SOPS_VERSION}/sops-${SOPS_VERSION}.linux" \
  && chmod 750 "/usr/local/bin/sops"

ARG VAULT_VERSION=1.7.0
RUN curl -Lo "./vault.zip" "https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip" \
  && unzip "./vault.zip" -d . \
  && mv "./vault" "/usr/local/bin/vault" \
  && chmod 750 "/usr/local/bin/vault" \
  && rm -rf "./vault.zip"

# ref https://github.com/jkroepke/helm-secrets/releases
ARG HELM_SECRETS_VERSION=v3.8.3
RUN helm plugin install "https://github.com/jkroepke/helm-secrets" --version "${HELM_SECRETS_VERSION}"

# ref https://github.com/databus23/helm-diff/releases
ARG HELM_DIFF_VERSION=v3.1.3
RUN helm plugin install "https://github.com/databus23/helm-diff" --version "${HELM_DIFF_VERSION}"

# ref https://github.com/kubernetes/kubectl/releases
# note: verify that the minor version is within 2+- of the version in use, do not ugprade beyond that
ARG KUBECTL_VERSION=v1.20.0
RUN curl -Lo "/usr/local/bin/kubectl" "https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl" \
  && chmod 750 "/usr/local/bin/kubectl"

ENTRYPOINT ["/bin/sh", "-c"]
